CFLAGS=--std=c18 -Wall -pedantic -Isrc/ -ggdb -Wextra -DDEBUG
CC=gcc

all:clean image_rotation

out/bmp.o: src/bmp.c
	$(CC) -c $(CFLAGS) $< -o $@

out/util.o: src/util.c
	$(CC) -c $(CFLAGS) $< -o $@

out/main.o: src/main.c
	$(CC) -c $(CFLAGS) $< -o $@

out/transformation.o: src/transformation.c
	$(CC) -c $(CFLAGS) $< -o $@

out/image.o: src/image.c
	$(CC) -c $(CFLAGS) $< -o $@

image_rotation: out/main.o out/util.o out/bmp.o out/transformation.o out/image.o
	$(CC) -o image_rotation.exe $^
run:
	./image_rotation.exe


clean:
	rm -f out/main.o out/util.o out/bmp.o out/transformation.o out/image.o image_rotation

