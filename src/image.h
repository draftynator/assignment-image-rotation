#ifndef IMAGE_ROTATION_MASTER_IMAGE_H
#define IMAGE_ROTATION_MASTER_IMAGE_H
#include <inttypes.h>
#include <stdlib.h>
struct pixel{
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

struct image{
    uint32_t height;
    uint32_t width;
    struct pixel* pixels;
};
void create_image(struct image* image, uint32_t width, uint32_t height);
void destroy_image(struct image* image);
#endif //IMAGE_ROTATION_MASTER_IMAGE_H
