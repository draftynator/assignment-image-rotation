
#ifndef IMAGE_ROTATION_MASTER_TRANSFORMATION_H
#define IMAGE_ROTATION_MASTER_TRANSFORMATION_H
#include "image.h"
struct image rotate( struct image*  const source );
#endif //IMAGE_ROTATION_MASTER_TRANSFORMATION_H
