#include <stdbool.h>
#include <stdio.h>

#include "bmp.h"
#include "transformation.h"

void usage() {
    fprintf(stderr, "Usage: ./print_header BMP_FILE_NAME\n"); 
}
static const char* const bmp_read_status_message[] = {
        [READ_OK] = "BMP read successfully",
        [READ_INVALID_SIGNATURE] = "Wrong arguments were passed to read_bmp function " ,
        [READ_FILE_OPENING_FAILED] = "Failed to open the file to read",
        [READ_INVALID_BITS] = "Pixel encoding is not compatible",
        [READ_INVALID_HEADER] = "Invalid header",
        [READ_PIXELS_LOSS] = "Some pixels were lost during reading",
        [READ_FILE_CLOSING_FAILED] = "Failed to close the file after reading"
};

static const char* const bmp_write_status_message[] = {
        [WRITE_OK] = "Written to BMP successfully",
        [WRITE_INVALID_SIGNATURE] = "Wrong arguments were passed to write_bmp function" ,
        [WRITE_FILE_OPENING_FAILED] = "Failed to open the file to write",
        [WRITE_FILE_CLOSING_FAILED] = "Failed to close the file after writing",
        [WRITE_PIXELS_LOSS] = "Some pixels were lost during writing",
        [WRITE_HEADER_ERROR] = "Failed to write header correctly"
};
int main(int argc, char** argv) {
    if (argc != 2) usage();
    if (argc < 2) err("Not enough arguments \n" );
    if (argc > 2) err("Too many arguments \n" );




    struct image my_image={0};
    enum bmp_read_status read_status= read_bmp(argv[1], &my_image);
    if (read_status) err(bmp_read_status_message[read_status]);
    print_err(bmp_read_status_message[read_status]);
    struct image rotated_image = rotate(&my_image);
    enum bmp_write_status write_status =  write_bmp(argv[1], &rotated_image);
    if (write_status) err(bmp_write_status_message[read_status]);
    print_err(bmp_write_status_message[write_status]);

    destroy_image(&my_image);
    destroy_image(&rotated_image);

    return 0;
}
