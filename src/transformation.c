#include "transformation.h"
#include <stdio.h>
struct image rotate(struct image* const  source ) {

    uint32_t rotated_width = source->height;
    uint32_t rotated_height=source->width;
    struct image rotated_image = {0};
    create_image(&rotated_image,rotated_width,rotated_height);
    fprintf(stdout, "%" PRIu32"\n", rotated_image.height);
    for (size_t i=0; i< (size_t)rotated_width;i++){

        for(size_t j=0; j<(size_t)rotated_height;++j){

            //rotated_image.pixels[j][rotated_width-1-i]=source->pixels[i];
            rotated_image.pixels[i+j*rotated_width]=source->pixels[rotated_height*(i+1)-j-1];

        }
    }

    return rotated_image;
}

