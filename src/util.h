#ifndef _UTIL_H_
#define _UTIL_H_
#include <stdint.h>
#include <stdio.h>
_Noreturn void err( const char* msg, ... );
uint32_t bmp_line_padding(uint32_t line_width);
void print_err (const char* const error_message);
#endif
