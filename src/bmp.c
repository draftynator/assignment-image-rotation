#include "bmp.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>

#define PRI_SPECIFIER(e) (_Generic( (e), uint16_t : "%" PRIu16, uint32_t: "%" PRIu32, default: "NOT IMPLEMENTED" ))
#define FOR_BMP_HEADER( FOR_FIELD ) \
        FOR_FIELD( uint16_t,bfType)\
        FOR_FIELD( uint32_t,bfileSize)\
        FOR_FIELD( uint32_t,bfReserved)\
        FOR_FIELD( uint32_t,bOffBits)\
        FOR_FIELD( uint32_t,biSize)\
        FOR_FIELD( uint32_t,biWidth)\
        FOR_FIELD( uint32_t,biHeight)\
        FOR_FIELD( uint16_t,biPlanes)\
        FOR_FIELD( uint16_t,biBitCount)\
        FOR_FIELD( uint32_t,biCompression)\
        FOR_FIELD( uint32_t,biSizeImage)\
        FOR_FIELD( uint32_t,biXPelsPerMeter)\
        FOR_FIELD( uint32_t,biYPelsPerMeter)\
        FOR_FIELD( uint32_t,biClrUsed)\
        FOR_FIELD( uint32_t,biClrImportant)

#define DECLARE_FIELD( t, n ) t n ;

struct __attribute__((packed)) bmp_header
{
    FOR_BMP_HEADER( DECLARE_FIELD )
};
#define PRINT_FIELD( t, name ) \
    fprintf( f, "%-17s: ",  # name ); \
    fprintf( f, PRI_SPECIFIER( header-> name ) , header-> name );\
    fprintf( f, "\n");


void bmp_header_print( struct bmp_header const* header, FILE* f ) {
   FOR_BMP_HEADER( PRINT_FIELD )
}



//READING
static bool read_header( FILE* f, struct bmp_header* header ) {
    return fread( header, sizeof( struct bmp_header ), 1, f );
}

bool read_header_from_file( const char* filename, struct bmp_header* header ) {
    if (!filename) return false;
    FILE* f = fopen( filename, "rb" ); 
    if (!f) return false;
    if (read_header( f, header ) ) {
        fclose( f );
        return true; 
    }
    fclose( f );
    return false;
}

enum bmp_read_status from_bmp( FILE* in, struct image* const read ){
    struct bmp_header header={0};
    if (read_header(in,&header)){
    read->height=header.biHeight;
    read->width=header.biWidth;
    }else return READ_INVALID_HEADER;
    if (header.biBitCount !=24) return READ_INVALID_BITS;
   // uint32_t pixels_count = header.biHeight * header.biWidth;
    create_image(read,header.biWidth,header.biHeight);
    struct pixel* current_address= read->pixels;
    size_t padding = (size_t)bmp_line_padding(header.biWidth);
    for (size_t i =0;i< (size_t)header.biHeight;i++){
        if(fread(current_address,sizeof(struct pixel)*header.biWidth,1,in) != 1) return READ_PIXELS_LOSS;
        fseek(in,padding , SEEK_CUR);
        current_address+=header.biWidth;
    }
   // if (successfully_read_pixels!=pixels_count) return READ_PIXELS_LOSS;
    return READ_OK;

}
enum bmp_read_status read_bmp(const char* filename, struct image* const read){
    if(read == NULL || filename==NULL) return READ_INVALID_SIGNATURE;
    FILE* f = fopen(filename, "rb");
    if (!f) return READ_FILE_OPENING_FAILED;
    enum bmp_read_status read_status = from_bmp(f,read);
    if (!fclose(f)) return read_status;
    return READ_FILE_CLOSING_FAILED;
}



//WRITING

void create_bmp_header(struct bmp_header* header, uint32_t width, uint32_t height) {
    header->bfType = 0x4D42;
    header->bfileSize = sizeof(struct bmp_header) + (sizeof(struct pixel)*width+bmp_line_padding(width))*height;
    header->bfReserved = 0;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = 40;
    header->biWidth = width;
    header->biHeight = height;
    header->biPlanes = 1;
    header->biBitCount = 24;
    header->biCompression = 0;
    header->biSizeImage = width*height*sizeof(struct pixel);
    header->biXPelsPerMeter = 0;
    header->biYPelsPerMeter = 0;
    header->biClrUsed = 0;
    header->biClrImportant = 0;
}

enum bmp_write_status to_bmp(FILE* out, struct image* const write){
    struct bmp_header header={0};
    create_bmp_header(&header,write->width,write->height);
    // uint32_t pixels_to_write = header.biHeight * header.biWidth;
    //uint32_t successfully_written_pixels=0;
    if(fwrite(&header, sizeof(struct bmp_header), 1, out)!=1) return WRITE_HEADER_ERROR;
    uint8_t filling_byte = 0x6F;
    struct pixel* current_adress= write->pixels;
    for (size_t i =0;i<(size_t)header.biHeight;i++){
        if (fwrite(current_adress,sizeof(struct pixel)*header.biWidth,1,out)!= 1) return WRITE_PIXELS_LOSS;
        fwrite(&filling_byte, sizeof(uint8_t), (size_t)bmp_line_padding(header.biWidth), out);
        current_adress+=header.biWidth;
    }
    //if (successfully_written_pixels!=pixels_to_write) return WRITE_PIXELS_LOSS;
    return WRITE_OK;
}
enum bmp_write_status write_bmp(const char* filename, struct image* const write){
    if(write == NULL || filename==NULL) return WRITE_INVALID_SIGNATURE;
    
    FILE* file = fopen(filename,"wb");
    if (!file) return WRITE_FILE_OPENING_FAILED;
    enum bmp_write_status write_status = to_bmp(file,write);
    if(!fclose(file)) return write_status;
    return WRITE_FILE_CLOSING_FAILED;

}
