#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "util.h"



_Noreturn void err( const char* msg, ... ) {
    va_list args;
    va_start (args, msg);
    vfprintf(stderr, msg, args);
    va_end (args);
    exit(1);
}



uint32_t bmp_line_padding(uint32_t line_width) {
    return line_width%4;
}

void print_err (const char* const error_message){
    fprintf(stderr, "%s\n", error_message);
}

