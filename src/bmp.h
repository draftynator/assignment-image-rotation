#ifndef _BMP_H_
#define _BMP_H_

#include <stdio.h>
#include <stdbool.h>
#include "image.h"
#include "util.h"


enum bmp_read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_FILE_OPENING_FAILED,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_PIXELS_LOSS,
    READ_FILE_CLOSING_FAILED

};
enum bmp_write_status {
    WRITE_OK = 0,
    WRITE_INVALID_SIGNATURE,
    WRITE_FILE_OPENING_FAILED,
    WRITE_FILE_CLOSING_FAILED,
    WRITE_PIXELS_LOSS,
    WRITE_HEADER_ERROR


};
//void bmp_header_print( struct bmp_header const* header, FILE* f );
//bool read_header_from_file( const char* filename, struct bmp_header* header );
enum bmp_read_status from_bmp( FILE* in, struct image* const read );
enum bmp_read_status read_bmp(const char* filename, struct image* const read);
//void create_bmp_header(struct bmp_header* header, uint32_t width, uint32_t height);
enum bmp_write_status to_bmp(FILE* out, struct image* const write);
enum bmp_write_status write_bmp(const char* filename, struct image* const write);


#endif
